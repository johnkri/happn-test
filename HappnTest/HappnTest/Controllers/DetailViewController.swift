//
//  MainTableViewController.swift
//  Happn-test
//
//  Created by John KRICORIAN on 06/09/2019.
//  Copyright © 2019 John KRICORIAN. All rights reserved.
//

import UIKit
import RxSwift

class DetailViewController: UIViewController {
    
    var selectedWeather: Weather?
    let disposeBag = DisposeBag()
    
    @IBOutlet weak var currentDateLabel: UILabel!
    @IBOutlet weak var currentWeatherImage: UIImageView!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    
    func poulateWithVM() {
        
        guard let weather = selectedWeather else { return }
        let vm = WeatherViewModel(weather)
        
        vm.dt.asDriver(onErrorJustReturn: "")
            .drive(currentDateLabel.rx.text)
            .disposed(by: disposeBag)
        
        vm.temperature.asDriver(onErrorJustReturn: "")
            .drive(temperatureLabel.rx.text)
            .disposed(by: disposeBag)
        
        vm.humidity.asDriver(onErrorJustReturn: "")
            .drive(humidityLabel.rx.text)
            .disposed(by: disposeBag)
        
        vm.foodImage.asDriver(onErrorJustReturn: #imageLiteral(resourceName: "default"))
            .drive(currentWeatherImage.rx.image)
            .disposed(by: disposeBag)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.prefersLargeTitles = true
        
        poulateWithVM()
    }
}
