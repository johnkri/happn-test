//
//  MainTableViewController.swift
//  Happn-test
//
//  Created by John KRICORIAN on 06/09/2019.
//  Copyright © 2019 John KRICORIAN. All rights reserved.
//

import Foundation
import UIKit
import RxSwift


class NewsTableViewController: UITableViewController {
    
    let disposeBag = DisposeBag()
    private var weatherListViewModel: WeatherListViewModel!
    private var weathers: [Weather]?
    
    private func populateNews() {
        let resource = Resource<ListResponse>(url: URL(string: ApiRoute.url)!)
        
        URLRequest.load(resource: resource)
            .subscribe(onNext: { weatherResponse in
                self.weathers = [weatherResponse.list[0]] + weatherResponse.list.enumerated().filter({($0.offset + 1) % 5 == 0}).map({$0.element})
                self.weatherListViewModel = WeatherListViewModel(self.weathers!)
                Persistence.saveWeatherToUserDefault(weathers: self.weathers!)
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }).disposed(by: disposeBag)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.prefersLargeTitles = true
        populateNews()
        
        if weathers == nil {
            self.weatherListViewModel = WeatherListViewModel(Persistence.loadWeatherFromUserData())
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.weatherListViewModel == nil ? 0 : weatherListViewModel.weatherViewModels.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "WeatherTableViewCell", for: indexPath) as? WeatherTableViewCell else {
            fatalError("WeatherTableViewCell is not found")
        }
        
        let weatherViewModel = weatherListViewModel.weatherAt(indexPath.row)
        
        weatherViewModel.dt.asDriver(onErrorJustReturn: "")
        .drive(cell.titleLabel.rx.text)
        .disposed(by: disposeBag)
        
        weatherViewModel.description.asDriver(onErrorJustReturn: "")
            .drive(cell.descriptionLabel.rx.text)
            .disposed(by: disposeBag)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let destinationVC = storyboard?.instantiateViewController(withIdentifier: "DetailViewController") as? DetailViewController else { return }
        
        let selectedWeather = weatherListViewModel.weatherViewModels[indexPath.row].weather
        destinationVC.selectedWeather = selectedWeather
        self.navigationController?.pushViewController(destinationVC, animated: true)
    }
}
