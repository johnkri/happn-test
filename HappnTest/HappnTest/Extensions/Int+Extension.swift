//
//  MainTableViewController.swift
//  Happn-test
//
//  Created by John KRICORIAN on 06/09/2019.
//  Copyright © 2019 John KRICORIAN. All rights reserved.
//

import Foundation

extension Int {
    func toStringDate() -> String
    {
        let date = Date(timeIntervalSince1970: TimeInterval(self))
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "E, d MMM yyyy HH:mm"
        let strDate = dateFormatter.string(from: date)
        
        return strDate
    }
    
    func toString() -> String
    {
        let myString = String(self)
        return myString
    }
}
