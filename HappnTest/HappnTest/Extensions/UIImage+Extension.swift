//
//  MainTableViewController.swift
//  Happn-test
//
//  Created by John KRICORIAN on 06/09/2019.
//  Copyright © 2019 John KRICORIAN. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    func getImageWith(string: String) -> UIImage {
        let url = URL(string: string)
        let data = try! Data(contentsOf: url!)
        return UIImage(data: data)!
    }
}
