//
//  MainTableViewController.swift
//  Happn-test
//
//  Created by John KRICORIAN on 06/09/2019.
//  Copyright © 2019 John KRICORIAN. All rights reserved.
//

import Foundation

extension String {
    
    func toData() -> Data {
        let url = URL(string: self)
        do {
            let data = try Data(contentsOf: url!)
            return data
            
        } catch let error {
            print(error.localizedDescription)
        }
        return Data()
    }
}
