//
//  MainTableViewController.swift
//  Happn-test
//
//  Created by John KRICORIAN on 06/09/2019.
//  Copyright © 2019 John KRICORIAN. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

struct WeatherListViewModel {
    let weatherViewModels: [WeatherViewModel]
}

extension WeatherListViewModel {
    
    init(_ weathers: [Weather]) {
        self.weatherViewModels = weathers.compactMap(WeatherViewModel.init)
    }
    
}

extension WeatherListViewModel {
    
    func weatherAt(_ index: Int) -> WeatherViewModel {
        return self.weatherViewModels[index]
    }
}

struct WeatherViewModel {
    
    let weather: Weather
    
    init(_ weather: Weather) {
        self.weather = weather
    }
    
}

extension WeatherViewModel {

    var icon: Observable<String> {
        return Observable<String>.just(weather.weather.first?.icon ?? "")
    }
    
    var description: Observable<String> {
        return Observable<String>.just(weather.weather.first?.description ?? "")
    }
    
    var dt: Observable<String> {
        let unixTimestamp = weather.dt.toStringDate()
        return Observable<String>.just(unixTimestamp)
    }
    
    var temperature: Observable<String> {
        let temp = "\(weather.main.temp.toCelsius().toString()) °"
        return Observable<String>.just(temp)
    }
    
    var humidity: Observable<String> {
        let humid = "\(weather.main.humidity.toString()) % d'humidité"
        return Observable<String>.just(humid)
    }
    
    var foodImage: Observable<UIImage> {
        let st = "http://openweathermap.org/img/w/" + (weather.weather[0].icon) + ".png"
        if let image = UIImage(data: st.toData()) {
            return Observable<UIImage>.just(image)
        } else {
            return Observable<UIImage>.just(#imageLiteral(resourceName: "default"))
        }
    }
}




