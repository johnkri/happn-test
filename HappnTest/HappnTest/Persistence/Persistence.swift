//
//  Persistence.swift
//  Happn-test
//
//  Created by John KRICORIAN on 06/09/2019.
//  Copyright © 2019 John KRICORIAN. All rights reserved.
//

import Foundation

class Persistence {
    
    static func saveWeatherToUserDefault(weathers: [Weather]) {
        let defaults = UserDefaults.standard
        defaults.set(try? PropertyListEncoder().encode(weathers), forKey: "weathers")
    }
    
    static func loadWeatherFromUserData() -> [Weather] {
        let defaults = UserDefaults.standard
        guard let playerData = defaults.object(forKey: "weathers") as? Data else {
            return [Weather]()
        }
        
        guard let weathers = try? PropertyListDecoder().decode([Weather].self, from: playerData) else {
            return [Weather]()
        }
        
        return weathers
    }
}
