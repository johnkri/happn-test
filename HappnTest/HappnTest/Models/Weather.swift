//
//  MainTableViewController.swift
//  Happn-test
//
//  Created by John KRICORIAN on 06/09/2019.
//  Copyright © 2019 John KRICORIAN. All rights reserved.
//

import Foundation


enum WeatherType: String, Codable {
    case clouds = "Clouds"
    case rain = "Rain"
    case clear = "Clear"
}

struct WeatherDetails: Codable {
    var id: Int
    var main: WeatherType
    var description: String
    var icon: String
}

struct Wind: Codable {
    var speed: Double
    var deg: Double
}

struct Clouds: Codable {
    var all: Int
}

struct Main: Codable {
    var temp: Double
    var temp_min: Double
    var temp_max: Double
    var pressure: Double
    var sea_level: Double
    var grnd_level: Double
    var humidity: Int
    var temp_kf: Double
    
    private enum CodingKeyes: String, CodingKey {
        case temp
        case temp_min
        case temp_max
        case pressure
        case sea_level
        case grnd_level
        case humidity
        case temp_kf
    }
}

struct Weather: Codable {
    var dt: Int
    var main: Main
    var clouds: Clouds
    var wind: Wind
    var weather: [WeatherDetails]
}


struct ListResponse: Codable {
    var list: [Weather]
    
}
