//
//  ApiRoute.swift
//  Happn-test
//
//  Created by John KRICORIAN on 06/09/2019.
//  Copyright © 2019 John KRICORIAN. All rights reserved.
//

import Foundation

class ApiRoute {
    enum Environment: String {
        case production = "https://api.openweathermap.org/data/2.5/forecast?"
    }
    
    enum CityID: String {
        case paris = "6455259"
    }
    
    
    static let url = "\(Environment.production.rawValue)id=\(CityID.paris.rawValue)&appid=\(ApiParameters.apiKey)"
}



